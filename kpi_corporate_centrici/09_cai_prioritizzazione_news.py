# Import libraries
import pandas as pd
import pickle
import numpy as np

# Import inputs
dtf_priorita = pd.read_excel("input/Prioritizzazione dal rank paese a priorità corporate_settore per bulk v_01.xlsx",
                            sheetname=0)

dtf_decodifica_paesi = pd.read_excel("input/dtf_decodifica_paese_20181205.xlsx",
                            sheetname=0)

dtf_anagrafica = pd.read_excel("input/Anagrafe con dettaglio possesso prodotti GTB 2018.xlsx",
                            sheetname=0, dtype='object')

pickle_off = open("input/dtf_news_corporate","rb")
dtf_news_corporate = pickle.load(pickle_off)
pickle_off.close()

pickle_off = open("input/dtf_profilazione_news","rb")
dtf_profilazione_news = pickle.load(pickle_off)
pickle_off.close()

# ID_3_11_1_NEWS_RELEVANCE_SCREENING
dtf_priorita = dtf_priorita.append(
    pd.DataFrame([[1, np.nan, 'Italy']], 
                 columns=dtf_priorita.columns))

dtf_decodifica_paesi.loc[
    dtf_decodifica_paesi.DESC_PAESE_QLIK.isnull(), 'DESC_PAESE_QLIK'] = dtf_decodifica_paesi.loc[
    dtf_decodifica_paesi.DESC_PAESE_QLIK.isnull(), 'DESC_PAESE_VISUALIZZAZIONE']

dtf_prioritizzazione = dtf_anagrafica.dropna(
    subset=['Paese di Residenza / Sede Legale (fonte AdG)']).merge(
    dtf_decodifica_paesi,
    right_on = 'DESC_PAESE_ANAGRAFICA',        
    left_on = 'Paese di Residenza / Sede Legale (fonte AdG)',        
    how='left').merge(
    dtf_priorita,
    left_on ='DESC_PAESE_QLIK',       
    right_on ='Paese',
    how='left').loc[:,
                    ['Codice SNDG',
                     'Gruppo (fonte CIB)',
                     'Bulk',
                     'DESC_PAESE_VISUALIZZAZIONE']]

dtf_prioritizzazione.Bulk.fillna(3, inplace=True)

dtf_prioritizzazione = dtf_prioritizzazione.merge(
    dtf_news_corporate,
    left_on = 'Codice SNDG',       
    right_on ='COD_SNDG',
    how = 'inner').loc[:,
                       ['index',
                        'title',
                        'content',
                        'Gruppo (fonte CIB)',
                        'Bulk']].drop_duplicates()

dtf_prioritizzazione.columns = ['DES_INDEX', 'DES_TITLE', 'DES_CONTENT',
                                'DES_GRUPPO', 'ID_3_11_1_NEWS_RELEVANCE_SCREENING']

dtf_prioritizzazione_priorita = dtf_prioritizzazione.loc[
    :,['DES_INDEX', 'DES_TITLE', 'DES_CONTENT', 'ID_3_11_1_NEWS_RELEVANCE_SCREENING']].drop_duplicates().groupby(
    ['DES_INDEX', 'DES_TITLE', 'DES_CONTENT'], as_index=False)['ID_3_11_1_NEWS_RELEVANCE_SCREENING'].min()

dtf_prioritizzazione_bi = dtf_prioritizzazione.iloc[:,:-1].merge(
    dtf_prioritizzazione_priorita,
    on = ['DES_INDEX', 'DES_TITLE', 'DES_CONTENT'],
    how = 'left').drop_duplicates()

dtf_prioritizzazione_visualizzazione = pd.DataFrame(
    dtf_prioritizzazione_bi.groupby(['DES_INDEX','DES_TITLE','DES_CONTENT',
                                    'ID_3_11_1_NEWS_RELEVANCE_SCREENING']
                                   )['DES_GRUPPO'].apply(list)).reset_index()

# Export news prioritization dataframes
dtf_prioritizzazione_bi.sort_values('ID_3_11_1_NEWS_RELEVANCE_SCREENING').to_excel(
    "output/dtf_news_relevance_screening_bi.xlsx",
    sheet_name='Result',
    index=False)

dtf_prioritizzazione_visualizzazione.sort_values('ID_3_11_1_NEWS_RELEVANCE_SCREENING').to_excel(
    "output/dtf_news_relevance_screening_visualization.xlsx",
    sheet_name='Result',
    index=False)