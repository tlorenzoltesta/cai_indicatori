# Set path
path<-getwd()
path_input<-paste0(path,"/input")
path_output<-paste0(path,"/output")

# Import libraries
library(readxl)
library(data.table)
library(dplyr)
library(stringr)
library(ggplot2)

# Import inputs
dtf_anagrafica_18 <- read_excel(paste0(path_input,"/Anagrafe con dettaglio possesso prodotti GTB 2018.xlsx"), guess_max = 10000)
dtf_incontri <- read_excel(paste0(path_input,"/Incontri.xls.xlsx"), guess_max = 25000)
dtf_opportunita <- read_excel(paste0(path_input,"/Opportunitą.xlsx"), guess_max = 25000)

#table INCONTRI
dtf_incontri$SNDG <- str_pad(dtf_incontri$SNDG, 16, pad = "0")
dtf_incontri<- rename(dtf_incontri, COD_SNDG='SNDG', Prodotti = 'Prodotti Presentati')

## ID_2_6_1_NUM_CONTACT_FREQUENCY
dtf_contatti_incontri<- dtf_incontri[,c('COD_SNDG', 'Data','Prodotti')]
dtf_contatti_incontri_2 <- unique(dtf_contatti_incontri)
dtf_contatti_incontri_2$Data<- as.Date(dtf_contatti_incontri_2$Data)
ID_2_6_1_NUM_CONTACT_FREQUENCY_INCONTRI_ANNO<- dtf_contatti_incontri_2 %>% 
  filter(Data>='2018-01-01' & Data <='2018-10-31') %>% 
  group_by(COD_SNDG) %>%
  summarise(ID_2_6_1_NUM_CONTACT_FREQUENCY_INCONTRI_ANNO = n())
ID_2_6_1_NUM_CONTACT_FREQUENCY_INCONTRI_LAST3M<- dtf_contatti_incontri_2 %>% 
  filter(Data >='2018-07-31' & Data <='2018-10-31') %>% 
  group_by(COD_SNDG) %>%
  summarise(ID_2_6_1_NUM_CONTACT_FREQUENCY_INCONTRI_LAST3M = n())

# ID_2_6_4_DAT_LAST_CONTACT_DATE_INCONTRI
dtf_last_date_incontri <- data.table(dtf_incontri)[,.(ID_2_6_4_DAT_LAST_CONTACT_DATE_INCONTRI = max(Data)),COD_SNDG]
ID_2_6_4_DAT_LAST_CONTACT_DATE_INCONTRI <- dtf_last_date_incontri %>%
  filter(ID_2_6_4_DAT_LAST_CONTACT_DATE_INCONTRI <= '2018-10-31')
# ID_2_6_4_DAT_LAST_CONTACT_DATE_INCONTRI_TF
dtf_last_date_incontri_tf <- data.table(dtf_incontri)[substr(Prodotti,1,2) == 'TF',.(ID_2_6_4_DAT_LAST_CONTACT_DATE_INCONTRI_TF = max(Data)),COD_SNDG]
ID_2_6_4_DAT_LAST_CONTACT_DATE_INCONTRI_TF <- dtf_last_date_incontri_tf %>%
  filter(ID_2_6_4_DAT_LAST_CONTACT_DATE_INCONTRI_TF <= '2018-10-31')
# ID_2_6_4_DAT_LAST_CONTACT_DATE_INCONTRI_CA
dtf_last_date_incontri_ca <- data.table(dtf_incontri)[substr(Prodotti,1,2) == 'CA',.(ID_2_6_4_DAT_LAST_CONTACT_DATE_INCONTRI_CA = max(Data)),COD_SNDG]
ID_2_6_4_DAT_LAST_CONTACT_DATE_INCONTRI_CA <- dtf_last_date_incontri_ca %>%
  filter(ID_2_6_4_DAT_LAST_CONTACT_DATE_INCONTRI_CA <= '2018-10-31')
# ID_2_6_4_DAT_LAST_CONTACT_DATE_INCONTRI_CSH
dtf_last_date_incontri_csh <- data.table(dtf_incontri)[substr(Prodotti,1,3) == 'CSH',.(ID_2_6_4_DAT_LAST_CONTACT_DATE_INCONTRI_CSH = max(Data)),COD_SNDG]
ID_2_6_4_DAT_LAST_CONTACT_DATE_INCONTRI_CSH <- dtf_last_date_incontri_csh %>%
  filter(ID_2_6_4_DAT_LAST_CONTACT_DATE_INCONTRI_CSH <= '2018-10-31')

#table OPPORTUNITA
dtf_opportunita <- rename(dtf_opportunita, COD_SNDG=`SNDG con zeri`,
                         Data = 'Data creazione',
                         Prodotti = 'Tipo Opportunitą')
dtf_opportunita$COD_SNDG[dtf_opportunita$COD_SNDG != 'WL'] <-
  str_pad(dtf_opportunita$COD_SNDG[dtf_opportunita$COD_SNDG !='WL'], 16, pad = "0")

## ID_2_6_1_NUM_CONTACT_FREQUENCY
dtf_contatti_opportunita<- dtf_opportunita[,c('COD_SNDG', 'Data','Prodotti')]
dtf_contatti_opportunita_2 <- unique(dtf_contatti_opportunita)
dtf_contatti_opportunita_2$Data<- as.Date(dtf_contatti_opportunita_2$Data)
ID_2_6_1_NUM_CONTACT_FREQUENCY_OPPORTUNITA_ANNO<- dtf_contatti_opportunita_2 %>% 
  filter(Data>='2018-01-01' & Data <='2018-10-31') %>% 
  group_by(COD_SNDG) %>%
  summarise(ID_2_6_1_NUM_CONTACT_FREQUENCY_OPPORTUNITA_ANNO = n())
ID_2_6_1_NUM_CONTACT_FREQUENCY_OPPORTUNITA_LAST3M<- dtf_contatti_opportunita_2 %>% 
  filter(Data >='2018-07-31' & Data <='2018-10-31') %>% 
  group_by(COD_SNDG) %>%
  summarise(ID_2_6_1_NUM_CONTACT_FREQUENCY_OPPORTUNITA_LAST3M = n())

# ID_2_6_4_DAT_LAST_CONTACT_DATE_OPPORTUNITA 
dtf_last_date_opportunita <- data.table(dtf_opportunita)[,.(ID_2_6_4_DAT_LAST_CONTACT_DATE_OPPORTUNITA = max(Data)),COD_SNDG]
ID_2_6_4_DAT_LAST_CONTACT_DATE_OPPORTUNITA <- dtf_last_date_opportunita %>%
  filter(ID_2_6_4_DAT_LAST_CONTACT_DATE_OPPORTUNITA<='2018-10-31')

dtf_opportunita$Prod=substr(dtf_opportunita$Prodotti,1,2)
dtf_opportunita$Prod[dtf_opportunita$Prod == 'CS'] <- substr(dtf_opportunita$Prodotti[
  dtf_opportunita$Prod == 'CS'],1,3)
dtf_last_contact_date_opportunita_2 <- data.table(dtf_opportunita)[order(Prod, COD_SNDG, Data, Macrofase, decreasing = TRUE),
                                                             .(Data, Macrofase, row = 1:.N),.(Prod, COD_SNDG)][row == 1]
# ID_2_6_4_DAT_LAST_CONTACT_DATE_OPPORTUNITA_TF
dtf_last_date_opportunita_tf <- dtf_last_contact_date_opportunita_2 %>%
  filter(substr(Prod,1,2) == 'TF') %>%
  select(COD_SNDG,ID_2_6_4_DAT_LAST_CONTACT_DATE_OPPORTUNITA_TF = Data) 
ID_2_6_4_DAT_LAST_CONTACT_DATE_OPPORTUNITA_TF <- dtf_last_date_opportunita_tf %>%
  filter(ID_2_6_4_DAT_LAST_CONTACT_DATE_OPPORTUNITA_TF <= '2018-10-31')
# ID_2_6_4_DAT_LAST_CONTACT_DATE_OPPORTUNITA_CA
dtf_last_date_opportunita_ca <- dtf_last_contact_date_opportunita_2 %>%
  filter(substr(Prod,1,2) == 'CA') %>%
  select(COD_SNDG,ID_2_6_4_DAT_LAST_CONTACT_DATE_OPPORTUNITA_CA = Data) 
ID_2_6_4_DAT_LAST_CONTACT_DATE_OPPORTUNITA_CA<- dtf_last_date_opportunita_ca %>%
  filter(ID_2_6_4_DAT_LAST_CONTACT_DATE_OPPORTUNITA_CA <= '2018-10-31')
# ID_2_6_4_DAT_LAST_CONTACT_DATE_OPPORTUNITA_CSH
dtf_last_date_opportunita_csh<- dtf_last_contact_date_opportunita_2 %>%
  filter(substr(Prod,1,3) == 'CSH') %>%
  select(COD_SNDG,ID_2_6_4_DAT_LAST_CONTACT_DATE_OPPORTUNITA_CSH = Data) 
ID_2_6_4_DAT_LAST_CONTACT_DATE_OPPORTUNITA_CSH <- dtf_last_date_opportunita_csh %>%
  filter(ID_2_6_4_DAT_LAST_CONTACT_DATE_OPPORTUNITA_CSH <= '2018-10-31')

# ID_2_6_5_NUM_OPEN_OPPORTUNITY_TF
dtf_open_opp_tf <- dtf_opportunita %>%
  filter((substr(Prod,1,2) == 'TF') & Macrofase == 'Aperta' & Data <= '2018-10-31') %>%
  select(COD_SNDG,DAT_CONTACT_DATE_TF = Data, Macrofase) 
ID_2_6_5_NUM_OPEN_OPPORTUNITY_TF <- dtf_open_opp_tf %>%
  group_by(COD_SNDG) %>%
  summarise(ID_2_6_5_NUM_OPEN_OPPORTUNITY_TF = n())
# ID_2_6_5_NUM_OPEN_OPPORTUNITY_CA
dtf_open_opp_ca <- dtf_opportunita %>%
  filter((substr(Prod,1,2) == 'CA') & Macrofase == 'Aperta' & Data <= '2018-10-31') %>%
  select(COD_SNDG,DAT_CONTACT_DATE_CA = Data, Macrofase) 
ID_2_6_5_NUM_OPEN_OPPORTUNITY_CA <- dtf_open_opp_ca %>%
  group_by(COD_SNDG) %>%
  summarise(ID_2_6_5_NUM_OPEN_OPPORTUNITY_CA = n())
# ID_2_6_5_NUM_OPEN_OPPORTUNITY_CSH
dtf_open_opp_csh <- dtf_opportunita %>%
  filter((substr(Prod,1,3) == 'CSH') & Macrofase == 'Aperta' & Data <= '2018-10-31') %>%
  select(COD_SNDG,DAT_CONTACT_DATE_CSH = Data, Macrofase) 
ID_2_6_5_NUM_OPEN_OPPORTUNITY_CSH <- dtf_open_opp_csh %>%
  group_by(COD_SNDG) %>%
  summarise(ID_2_6_5_NUM_OPEN_OPPORTUNITY_CSH = n())

# ID_2_6_6_NUM_WON_OPPORTUNITY_TF
dtf_won_opp_tf <- dtf_opportunita %>%
  filter((substr(Prod,1,2) == 'TF') & Macrofase == 'Vinta' & Data <= '2018-10-31') %>%
  select(COD_SNDG,DAT_CONTACT_DATE_TF = Data, Macrofase) 
ID_2_6_6_NUM_WON_OPPORTUNITY_TF <- dtf_won_opp_tf %>%
  group_by(COD_SNDG) %>%
  summarise(ID_2_6_6_NUM_WON_OPPORTUNITY_TF = n())
# ID_2_6_6_NUM_WON_OPPORTUNITY_CA
dtf_won_opp_ca <- dtf_opportunita %>%
  filter((substr(Prod,1,2) == 'CA') & Macrofase == 'Vinta' & Data <= '2018-10-31') %>%
  select(COD_SNDG,DAT_CONTACT_DATE_CA = Data, Macrofase) 
ID_2_6_6_NUM_WON_OPPORTUNITY_CA <- dtf_won_opp_ca %>%
  group_by(COD_SNDG) %>%
  summarise(ID_2_6_6_NUM_WON_OPPORTUNITY_CA = n())
# ID_2_6_6_NUM_WON_OPPORTUNITY_CSH
dtf_won_opp_csh <- dtf_opportunita %>%
  filter((substr(Prod,1,3) == 'CSH') & Macrofase == 'Vinta' & Data <= '2018-10-31') %>%
  select(COD_SNDG,DAT_CONTACT_DATE_CSH = Data, Macrofase) 
ID_2_6_6_NUM_WON_OPPORTUNITY_CSH <- dtf_won_opp_csh %>%
  group_by(COD_SNDG) %>%
  summarise(ID_2_6_6_NUM_WON_OPPORTUNITY_CSH = n())

#ID_2_6_7_WON_OPPORTUNITY_DATE_CSH
prova_2_6_7 <- dtf_won_opp_csh %>%
  count(DAT_CONTACT_DATE_CSH) 

ggplot(data = prova_2_6_7, aes(x = DAT_CONTACT_DATE_CSH,y=n)) + geom_line()

dtf_won_opp_csh <- dtf_won_opp_csh %>%
  mutate(ID_2_6_7_WON_OPPORTUNITY_DATE_CSH =
           cut(DAT_CONTACT_DATE_CSH,
               c(as.POSIXct('2014-12-31'),as.POSIXct('2016-12-31'),
                 as.POSIXct('2017-12-31'),as.POSIXct('2019-12-31')),
               labels=c("<2017", "2017", "2018")))
               

# ID_2_6_8_NUM_LOST_OPPORTUNITY_TF
dtf_lost_opp_tf <- dtf_opportunita %>%
  filter((substr(Prod,1,2) == 'TF') & Macrofase == 'Persa' & Data <= '2018-10-31') %>%
  select(COD_SNDG,DAT_CONTACT_DATE_TF = Data, Macrofase) 
ID_2_6_8_NUM_LOST_OPPORTUNITY_TF <- dtf_lost_opp_tf %>%
  group_by(COD_SNDG) %>%
  summarise(ID_2_6_8_NUM_LOST_OPPORTUNITY_TF = n())
# ID_2_6_8_NUM_LOST_OPPORTUNITY_CA
dtf_lost_opp_ca <- dtf_opportunita %>%
  filter((substr(Prod,1,2) == 'CA') & Macrofase == 'Persa' & Data <= '2018-10-31') %>%
  select(COD_SNDG,DAT_CONTACT_DATE_CA = Data, Macrofase) 
ID_2_6_8_NUM_LOST_OPPORTUNITY_CA <- dtf_lost_opp_ca %>%
  group_by(COD_SNDG) %>%
  summarise(ID_2_6_8_NUM_LOST_OPPORTUNITY_CA = n())
# ID_2_6_8_NUM_LOST_OPPORTUNITY_CSH
dtf_lost_opp_csh <- dtf_opportunita %>%
  filter((substr(Prod,1,3) == 'CSH') & Macrofase == 'Persa' & Data <= '2018-10-31') %>%
  select(COD_SNDG,DAT_CONTACT_DATE_CSH = Data, Macrofase) 
ID_2_6_8_NUM_LOST_OPPORTUNITY_CSH <- dtf_lost_opp_csh %>%
  group_by(COD_SNDG) %>%
  summarise(ID_2_6_8_NUM_LOST_OPPORTUNITY_CSH = n())

# Join 
dtf_salesforce <- select(dtf_anagrafica_18, COD_SNDG = "Codice SNDG") %>%
  left_join(ID_2_6_1_NUM_CONTACT_FREQUENCY_INCONTRI_ANNO, by = "COD_SNDG") %>%
  left_join(ID_2_6_1_NUM_CONTACT_FREQUENCY_INCONTRI_LAST3M, by = "COD_SNDG") %>%
  left_join(ID_2_6_1_NUM_CONTACT_FREQUENCY_OPPORTUNITA_ANNO, by = "COD_SNDG") %>%
  left_join(ID_2_6_1_NUM_CONTACT_FREQUENCY_OPPORTUNITA_LAST3M, by = "COD_SNDG")
dtf_salesforce[is.na(dtf_salesforce)] <- 0
  
dtf_salesforce <- dtf_salesforce %>%
  left_join(ID_2_6_4_DAT_LAST_CONTACT_DATE_INCONTRI, by = "COD_SNDG") %>%
  left_join(ID_2_6_4_DAT_LAST_CONTACT_DATE_INCONTRI_TF, by = "COD_SNDG") %>%
  left_join(ID_2_6_4_DAT_LAST_CONTACT_DATE_INCONTRI_CA, by = "COD_SNDG") %>%
  left_join(ID_2_6_4_DAT_LAST_CONTACT_DATE_INCONTRI_CSH, by = "COD_SNDG") %>%
  left_join(ID_2_6_4_DAT_LAST_CONTACT_DATE_OPPORTUNITA, by = "COD_SNDG") %>%
  left_join(ID_2_6_4_DAT_LAST_CONTACT_DATE_OPPORTUNITA_TF, by = "COD_SNDG") %>%
  left_join(ID_2_6_4_DAT_LAST_CONTACT_DATE_OPPORTUNITA_CA, by = "COD_SNDG") %>%
  left_join(ID_2_6_4_DAT_LAST_CONTACT_DATE_OPPORTUNITA_CSH, by = "COD_SNDG") %>%
  
  left_join(ID_2_6_5_NUM_OPEN_OPPORTUNITY_TF, by = "COD_SNDG") %>%
  left_join(ID_2_6_5_NUM_OPEN_OPPORTUNITY_CA, by = "COD_SNDG") %>%
  left_join(ID_2_6_5_NUM_OPEN_OPPORTUNITY_CSH, by = "COD_SNDG") %>%

  left_join(ID_2_6_6_NUM_WON_OPPORTUNITY_TF, by = "COD_SNDG") %>%
  left_join(ID_2_6_6_NUM_WON_OPPORTUNITY_CA, by = "COD_SNDG") %>%
  left_join(ID_2_6_6_NUM_WON_OPPORTUNITY_CSH, by = "COD_SNDG") %>%
  
  left_join(ID_2_6_8_NUM_LOST_OPPORTUNITY_TF, by = "COD_SNDG") %>%
  left_join(ID_2_6_8_NUM_LOST_OPPORTUNITY_CA, by = "COD_SNDG") %>%
  left_join(ID_2_6_8_NUM_LOST_OPPORTUNITY_CSH, by = "COD_SNDG")

# Export outputs
saveRDS(dtf_salesforce, file = paste0(path_output,"/dtf_incontri_opportunita_r1.rds"))

