# Set path
path<-getwd()
path_input<-paste0(path,"/input")
path_output<-paste0(path,"/output")


# Import libraries
library(dplyr)
library(readxl)


# Import inputs
dtf_anagrafica_18 <- read_excel(paste0(path_input,
                                       "/Anagrafe con dettaglio possesso prodotti GTB 2018.xlsx"),
                                guess_max = 10000)
dtf_garanzie_internazionali <- read_excel(paste0(path_input,
                                                 "/Garanzie Int. CIB 201708_201807.xlsx"), 
                                          guess_max = 10000)
dtf_lettere_credito <- read_excel(paste0(path_input,
                                         "/Lettere di Credito CIB 201708_201807.xlsx"), 
                                  guess_max = 10000)


# ID_1_1_5_DESC_COMMERCIAL_RELATIONSHIP
dtf_a_gi = dtf_anagrafica_18 %>%
  inner_join(dtf_garanzie_internazionali, by = c("Codice SNDG" = "SUPERNDG")) %>%
  select(COD_SNDG = "Codice SNDG", ID_1_1_5_DESC_COMMERCIAL_RELATIONSHIP = "NAZIONE_QLIK") 
dtf_a_lc = dtf_anagrafica_18 %>%
  inner_join(dtf_lettere_credito, by = c("Codice SNDG" = "SUPERNDG")) %>%
  select(COD_SNDG = "Codice SNDG", ID_1_1_5_DESC_COMMERCIAL_RELATIONSHIP = "NAZIONE_QLIK") 
dtf_commercial_relationship <- unique(dplyr::bind_rows(dtf_a_gi, dtf_a_lc))
dtf_commercial_relationship_nonnull <- dtf_commercial_relationship[complete.cases(dtf_commercial_relationship),]


# ID_1_1_5_NUM_COMMERCIAL_RELATIONSHIP
dtf_numero_commercial_relationship <- dtf_commercial_relationship_nonnull %>%
  group_by(COD_SNDG) %>%
  summarise(ID_1_1_5_NUM_COMMERCIAL_RELATIONSHIP = n()) %>%
  right_join(dtf_anagrafica_18[,'Codice SNDG'], by = c('COD_SNDG' = 'Codice SNDG'))
dtf_numero_commercial_relationship[is.na(dtf_numero_commercial_relationship)] <- 0


# Export outputs
saveRDS(dtf_commercial_relationship_nonnull, file=paste0(path_output,
                                                         "/dtf_commercial_relationship_r1.rds"))
saveRDS(dtf_numero_commercial_relationship, file=paste0(path_output,
                                                        "/dtf_numero_commercial_relationship_r1.rds"))
