# Import libraries
import pickle
import sklearn
from sklearn.feature_extraction.text import CountVectorizer
import nltk
from nltk.corpus import stopwords
import pandas as pd
import numpy as np
import re

###################################################### Configuration input #############################################
path = '' # rawDocuments_list (from Stream Prodotti Trade) folder path
########################################################################################################################

# Import inputs
pickle_off = open(path+"/rawDocuments_list","rb")
lst_dct_nib_text = pickle.load(pickle_off)
pickle_off.close()

dtf_anagrafica = pd.read_excel(
    'Anagrafe con dettaglio possesso prodotti GTB 2018.xlsx', dtype = object)

# ID_1_3_4_DESC_FILIERA
lst_settori_ricerca = list(set([i for i in list(dtf_anagrafica['Ateco (fonte AdG)']) if i == i]))

lst_sw = stopwords.words('italian')

vectorizer = CountVectorizer(lowercase=True, stop_words=lst_sw, ngram_range=(1, 6), binary=True)

mtr_tfidf_nib = vectorizer.fit_transform([i['text'] for i in lst_dct_nib_text])

mtr_tfidf_settori= vectorizer.transform(lst_settori_ricerca)

vectorizer2 = CountVectorizer(lowercase=True, stop_words=lst_sw, ngram_range=(1, 6), binary=True)

mtr_tfidf_settori_2 = vectorizer2.fit_transform(lst_settori_ricerca)

dtf_num_parole_settore = pd.DataFrame(np.sum(mtr_tfidf_settori_2, axis=1),
                                   index=lst_settori_ricerca, columns=['NUM_PAROLE_SETTORE']).T

mtr_filiera = np.dot(mtr_tfidf_nib,mtr_tfidf_settori.transpose()).todense()

dtf_filiera = pd.DataFrame(mtr_filiera, index=[i['vat_number'] for i in lst_dct_nib_text], columns=lst_settori_ricerca)

dtf_filiera_percentuale = dtf_filiera.div(dtf_num_parole_settore.iloc[0])

dtf_num_parole_settore = pd.DataFrame(np.sum(mtr_tfidf_settori_2, axis=1),
                                   index=lst_settori_ricerca, columns=['NUM_PAROLE_SETTORE']).T

mtr_filiera = np.dot(mtr_tfidf_nib,mtr_tfidf_settori.transpose()).todense()

dtf_filiera = pd.DataFrame(mtr_filiera, index=[i['vat_number'] for i in lst_dct_nib_text], columns=lst_settori_ricerca)

dtf_filiera_percentuale = dtf_filiera.div(dtf_num_parole_settore.iloc[0])

dtf_filiera_percentuale_2 = (dtf_filiera_percentuale==1).astype(int)

dtf_filiera_percentuale_3 = dtf_filiera_percentuale_2.stack().reset_index()

dtf_filiera_percentuale_4 = dtf_filiera_percentuale_3[dtf_filiera_percentuale_3[0]==1].iloc[:,:2]

# ID_1_4_1_FLG_PRESENZA_TESORERIA_ACCENTRATA
for elem in lst_dct_nib_text:
    elem['valore_tesoreriaAccentrata'] = re.findall(r'gestione accentrata della tesoreria\s+[1-9]\d*\b',
                                                   elem['text'].lower().replace('.', ''))
    

dtf_tesoreria_accentrata = pd.DataFrame([(i['vat_number'], 1) for i in lst_dct_nib_text
                                         if i['valore_tesoreriaAccentrata'] !=[]] +
                                        [(i['vat_number'], 0) for i in lst_dct_nib_text
                                         if i['valore_tesoreriaAccentrata'] ==[]]
                                        )

dtf_tesoreria_accentrata.columns = ['COD_FISCALE','FLG_TESORERIA_ACCENTRATA']

# Export outputs
dtf_filiera_percentuale_4.to_csv('/output/dtf_filera.csv', header = ["Codice Fiscale", "ID_DESC_FILIERA"], index=False)
dtf_tesoreria_accentrata.to_csv('/output/dtf_tesoreria_accentrata.csv', index=False)