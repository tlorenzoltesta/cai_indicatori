# Import libraries
import pandas as pd
from newsapi import NewsApiClient
import pickle
import math

# Keys for Developer News API account
k1 = NewsApiClient(api_key='b9eb50d6898c4d61adea105a9759d21b') #####
k2 = NewsApiClient(api_key='0500e6475117470887e11bf3e06cb1ed') #####
k3 = NewsApiClient(api_key='27d05fbd606b4b53b3b85630f30ced02') #####

# Import keywords
dtf_keyword = pd.read_excel("input/Lista Keywords_Country  Business Priority_v03.xlsx",
                            sheetname=0)
lst_kw = list(dtf_keyword.iloc[:,-2])

# Retrieve news from News API 
key = k1
dct_articoli = {}
counter = 0
for keyword in lst_kw:
    dct_articoli[keyword] = {}
    dct_articoli[keyword][1] = key.get_everything(q=keyword, sources='reuters', sort_by='relevancy',
                                                          language='en', page_size=100, page=1)
    pages = min((math.ceil(dct_articoli[keyword][1]['totalResults']/100)), 10)
    for page in range(2,pages+1):
        dct_articoli[keyword][page] = key.get_everything(q=keyword, sources='reuters', sort_by='relevancy',
                                                          language='en', page_size=100, page=page)
    counter += pages
    if counter > 200:
        key = k2
    if counter > 400:
        key = k3

# Convert news dictionary in dataframe 
lst_all_information = []
search_nr = 1
for k_1, v_1 in dct_articoli.items():
    for k_2, v_2 in v_1.items():
        for article in v_2['articles']:
            lst_all_information.append((search_nr, v_2['totalResults'], k_1, article['author'], article['content'],
                                        article['description'], article['publishedAt'], article['source']['name'],
                                        article['title'], article['url'], article['urlToImage']))
    search_nr += 1

dtf_all_information = pd.DataFrame(lst_all_information,
                            columns=['nrSearch', 'totalResults', 'keyword', 'author', 'content', 'description',
                            'publishedAt', 'sourceName', 'title', 'url', 'urlToImage'])

# Export news dataframe
pickling_on = open("output/dtf_all_news","wb")
pickle.dump(dtf_all_information, pickling_on)
pickling_on.close()