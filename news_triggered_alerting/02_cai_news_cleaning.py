# Import libraries
import pandas as pd
import re
import pickle

# Import news dataframe
pickle_off = open("output/dtf_all_news","rb")
dtf_all_news = pickle.load(pickle_off)
pickle_off.close()

# Delete older duplicate news and e reshaping dataframe
dtf_all_news = dtf_all_news.fillna(-1).reset_index()

dtf_all_news['title_cleaned'] = dtf_all_news['title'].str.lower().str.replace('[^a-zA-Z0-9]', ' ').str.replace('\s+', ' ')                                .str.replace('update \d+', '')                                .str.strip()

dtf_all_news.loc[dtf_all_news['content'] == -1,'content'] = dtf_all_news['description']

dtf_all_news.loc[
    dtf_all_news['content'].str.contains("the news and media division of Thomson Reuters", 
                                         na=False),'content'] = dtf_all_news['description']

dtf_all_news = dtf_all_news[dtf_all_news['title'].str.contains("Morning News Call") == False]

dtf_news_cleaned_1 = dtf_all_news[
    dtf_all_news.groupby(['title_cleaned'])['publishedAt'].transform(max) == dtf_all_news['publishedAt']]

dtf_news_cleaned_2 = dtf_news_cleaned_1.loc[
    :,['keyword', 'content', 'publishedAt', 'title', 'title_cleaned', 'sourceName']]

dummy = pd.get_dummies(dtf_news_cleaned_2['keyword'])

dtf_news_cleaned_3 = pd.concat([dtf_news_cleaned_2, dummy], axis=1)

dtf_news_cleaned_4 = dtf_news_cleaned_3.groupby(
    ['content', 'publishedAt', 'title', 'title_cleaned', 'sourceName'],
    as_index=False).sum().reset_index()

dtf_news_cleaned_5 = dtf_news_cleaned_4[
    dtf_news_cleaned_4.groupby(['title_cleaned'])['index'].transform(max) == dtf_news_cleaned_4['index']]

dtf_news_cleaned_6 = dtf_news_cleaned_5.drop('index', axis=1).copy()

for i in list(dtf_news_cleaned_6.columns)[5:]:
    dtf_news_cleaned_6.loc[dtf_news_cleaned_6[i] > 1, i] = 1

dtf_news_cleaned_7 = dtf_news_cleaned_6[
    dtf_news_cleaned_6.groupby(['content'])['publishedAt'].transform(max) == dtf_news_cleaned_6['publishedAt']
].reset_index()

dtf_news_cleaned_8 = dtf_news_cleaned_7[
    dtf_news_cleaned_7.groupby(['content'])['index'].transform(max) == dtf_news_cleaned_7['index']]

dtf_news_cleaned_9 = dtf_news_cleaned_8.drop('index', axis=1).reset_index(drop=True).reset_index()

dtf_news_cleaned_9['index'] = dtf_news_cleaned_9['index']+1 

# Export processed news dataframe
pickling_on = open("output/dtf_data_newsapi","wb")
pickle.dump(dtf_news_cleaned_9, pickling_on)
pickling_on.close()