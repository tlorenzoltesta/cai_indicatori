# Import libraries
import pandas as pd
import pickle
import re
from nltk import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
wordnet_lemmatizer = WordNetLemmatizer()
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import string

# Define functions
def contentCleaned(text):
    """
    contentCleaned selects the relevant part of the article content

    :param text: String containing article content
    :return: String containing the relevant part of the article content
    """
    try:
        return re.search(r'\(.*Reuters.*\) [\-\–] (.*?) \[\+', text).group(1)
    except AttributeError:
        return ''

def titleCleaned(text):
    """
    titleCleaned selects the relevant part of the article title

    :param text: String containing article tile
    :return: String containing the relevant part of the article title
    """
    text_1 = text
    text_1 = re.sub(r'\bUPDATE\b[\s]?\d+[\s]?\-', '' , text_1)
    text_1 = re.sub(r'\bTABLE\b[\s]?\-', '' , text_1)
    text_1 = re.sub(r'\bCORRECTED\b[\s]?[\-\:][\s]?', '' , text_1)
    text_1 = re.sub(r'\bRPT\b[\s]?[\-][\s]?', '' , text_1)
    text_1 = re.sub(r'\bREFILE\b[\s]?[\-][\s]?', '' , text_1)
    text_1 = re.sub(r'\bCOLUMN\b[\s]?[\-][\s]?', '' , text_1)
    return text_1

def CleanAndLemm(text):
    """
    CleanAndLemm cleans and lemmatizes text

    :param text: String containing text
    :return: String containing the cleaned and lemmatized text
    """
    text = re.sub(r'[\-\–]', ' ' , text)   
    tokens = word_tokenize(text)
    tokens = [w.lower() for w in tokens]
    tokens = [re.sub(r'\.', '' , w) for w in tokens]
    table = str.maketrans('', '', string.punctuation)
    stripped = [w.translate(table) for w in tokens]
    words = [word for word in stripped if word.isalpha()]
    stop_words = set(stopwords.words('english'))
    words = [w for w in words if not w in stop_words]
    return ' '.join(wordnet_lemmatizer.lemmatize(w) for w in words)

def checkAggiornamento(dtf, soglia_inf=0, soglia_sup=1):
    """
    checkAggiornamento returns a dataframe containing news title and content and list and number of the
    most recent news indices with similarity similarity score between the specified thresholds
    
    :param dtf: Dataframe 
    :param soglia_inf: Lower threshold
    :param soglia_sup: Upper threshold
    :return: Dataframe with duplicate news
    """    
    resultTuple_list = []
    for i in range(len(dtf)):
        lista_duplicati = [k for k in list(
            dtf.columns[(dtf >= soglia_inf).iloc[i] & (dtf <= soglia_sup).iloc[i]])[2:] if k > i]
        resultTuple_list.append(
            (dtf.loc[i,'title']
             ,dtf.loc[i,'content']
             ,lista_duplicati
             ,len(lista_duplicati)
            )
        )
    return pd.DataFrame(resultTuple_list)

# Import the already processed news dataframe
pickle_off = open("output/dtf_data_newsapi","rb")
dtf_data = pickle.load(pickle_off)
pickle_off.close()

# Delete less recent news with a similarity score over 0.9
dtf_data_1 = dtf_data.sort_values('publishedAt').reset_index(drop=True)

lst_corpus_ricerca = [titleCleaned(a)+'. '+contentCleaned(b) for a,b in zip(dtf_data_1['title'], dtf_data_1['content'])]

lst_corpus_similarita = [CleanAndLemm(i) for i in lst_corpus_ricerca]

vectorizer = CountVectorizer(lowercase=False)

dtf_similarita = vectorizer.fit_transform(lst_corpus_similarita)

dtf_similarita_coseno = cosine_similarity(dtf_similarita,dtf_similarita, dense_output=True)

dtf_similarita_coseno_1 = pd.DataFrame(dtf_similarita_coseno)

dtf_similarita_coseno_2 = pd.concat([dtf_data_1[['title', 'content']], dtf_similarita_coseno_1], axis=1)

dtf_similarita_90=checkAggiornamento(dtf_similarita_coseno_2,0.9,1)

dtf_data_no_duplicati = dtf_similarita_90[dtf_similarita_90[3]==0].iloc[:,[0,1]]

dtf_data_no_duplicati_1 = dtf_data_no_duplicati.merge(dtf_data_1, how='left', left_on=[0,1],
                                                      right_on=['title','content']).drop([0,1], axis=1)

# Export the processed news dataframe without duplicates
pickling_on = open("output/dtf_data_newsapi_no_duplicati","wb")
pickle.dump(dtf_data_no_duplicati_1, pickling_on)
pickling_on.close()