# Import libraries
import os
import pickle
import pandas as pd
import numpy as np
from cleanco import cleanco
import re
import spacy
from spacy import displacy
import en_core_web_sm
nlp = en_core_web_sm.load()
from sklearn.feature_extraction.text import CountVectorizer

# Define functions
def contentCleaned(text):
    """
    contentCleaned selects the relevant part of the article content

    :param text: String containing article content
    :return: String containing the relevant part of the article content
    """
    try:
        return re.search(r'\(.*Reuters.*\) [\-\–] (.*?) \[\+', text).group(1)
    except AttributeError:
        return ''

def titleCleaned(text):
    """
    titleCleaned selects the relevant part of the article title

    :param text: String containing article tile
    :return: String containing the relevant part of the article title
    """
    text_1 = text
    text_1 = re.sub(r'\bUPDATE\b[\s]?\d+[\s]?\-', '' , text_1)
    text_1 = re.sub(r'\bTABLE\b[\s]?\-', '' , text_1)
    text_1 = re.sub(r'\bCORRECTED\b[\s]?[\-\:][\s]?', '' , text_1)
    text_1 = re.sub(r'\bRPT\b[\s]?[\-][\s]?', '' , text_1)
    text_1 = re.sub(r'\bREFILE\b[\s]?[\-][\s]?', '' , text_1)
    text_1 = re.sub(r'\bCOLUMN\b[\s]?[\-][\s]?', '' , text_1)
    return text_1

def NER(text):
    """
    NER implement named-entity recognition process over a string

    :param text: Input string
    :return: Token classified and classification label tuples list
    """
    doc = nlp(text)
    return [(X.text, X.label_) for X in doc.ents]

# Import the processed news dataframe without duplicates
pickle_off = open("output/dtf_data_newsapi_no_duplicati","rb")
dtf_data = pickle.load(pickle_off)
pickle_off.close()

# Import anagraphic dataframe
dtf_anagrafica = pd.read_excel(
    'input/Anagrafe con dettaglio possesso prodotti GTB 2018.xlsx', dtype = object)

# Clean corporate group name
lst_nomi_gruppo = list(dtf_anagrafica['Gruppo (fonte CIB)'])

lst_nomi_gruppo_2 = [re.sub(r'\bgruppo\b|\bgroup\b|\bin liquidazione\b', '', i.lower().strip())
                         for i in lst_nomi_gruppo]

lst_nomi_gruppo_3 = [re.sub(r'(\(.*)', '', i.lower().strip()) for i in lst_nomi_gruppo_2]

lst_nomi_gruppo_4 = []
for name in lst_nomi_gruppo_3:
    for n in range(4):
        name = cleanco(name).clean_name()
    lst_nomi_gruppo_4.append(name)

regex = [
    r'\bs[ \.]?r[ \.]?((l\.\s)|(l\s)|(l$))',
    r'\bk[ \.]?f[ \.]?((t\.\s)|(t\s)|(t$))',    
    r'\bs[ \.]{0,2}r[ \.]{0,2}((o\.\s)|(o\s)|(o$))',
    r'\bs[ \.]?p[ \.]?((a\.\s)|(a\s)|(a$))',
    r'\bb[ \.]?((v\.\s)|(v\s)|(v$))',
    r'\bc[ \.]?((o\.\s)|(o\s)|(o$))',
    r'\ba[ \.]?((s\.\s)|(s\s)|(s$))',
    r'\bp[ \.]?c[ \.]?((l\.\s)|(l\s)|(l$))',
    r'\bs[ \.]?a[ \.]?((s\.\s)|(s\s)|(s$))',
    r'\bs[ \.]?((a\.\s)|(a\s)|(a$))',
    r'\bb[ \.]?s[ \.]?((c\.\s)|(c\s)|(c$))',
    r'\bs[ \.]?((l\.\s)|(l\s)|(l$))',
    r'\bs[ \.]?((e\.\s)|(e\s)|(e$))',
    r'\bsp[ \.]{0,3}z[ \.]?o[ \.]?((o\.\s)|(o\s)|(o$))',
    r'\bi[ \.]?n[ \.]?((c\.\s)|(c\s)|(c$))',
    r'\bco[ \.\,]{0,3}?lt((d\.\s)|(d\s)|(d$))',
    r'\d{5,}',
    r'\bs[ \.]?a[ \.]?b[ \.]{0,3}d[ \.]?((e\.\s)|(e\s)|(e$))',
    r'\bm[ \.]?b[ \.]?((h\.\s)|(h\s)|(h$))',
    r'\bg[ \.]?m[ \.]?b[ \.]?h[ \.]?&[ \.]{0,3}c[ \.]?o[ \.]?k[ \.]?((g\.\s)|(g\s)|(g$))',
    r'\bg[ \.]?m[ \.]?b[ \.]?((h\.\s)|(h\s)|(h$))',
    r'\bs[ \.]?g[ \.]?f[ \.]?((t\.\s)|(t\s)|(t$))',
    r'\bs[ \.]?h[ \.]?p[ \.]?((k\.\s)|(k\s)|(k$))',
    r'\bs[ \.]?c[ \.]?a[ \.]?r[ \.]?((l\.\s)|(l\s)|(l$))',
    r'societa\' consortile a r.l',
    r's.p.a.,',
    r'inc_',
    r'_gr_mc_',
    r'[_,/*]+',
    r'[\s]+'
]

lst_nomi_gruppo_puliti=[]
for name in lst_nomi_gruppo_4:
    name_1 = name
    for reg in regex:
        name_1 = re.sub(reg, ' ', name_1)
    lst_nomi_gruppo_puliti.append(name_1.strip())

dtf_anagrafica['Gruppo (fonte CIB) lower'] = dtf_anagrafica['Gruppo (fonte CIB)'].str.lower()

dtf_anagrafica['Gruppo (fonte CIB) pulito'] = lst_nomi_gruppo_puliti

dtf_anagrafica['Gruppo (fonte CIB) modificato'] = (
    dtf_anagrafica['Gruppo (fonte CIB) lower'] != dtf_anagrafica['Gruppo (fonte CIB) pulito']).astype(int)

dtf_nome_gruppo_pulito = dtf_anagrafica[['Gruppo (fonte CIB)',
                                         'Gruppo (fonte CIB) pulito',
                                         'Gruppo (fonte CIB) modificato']].copy()

# News profiling with respect to the corporate group name
dtf_data.reset_index(drop=True, inplace=True)

lst_corpus_per_ricerca = [titleCleaned(a)+'. '+contentCleaned(b) for a,b in zip(dtf_data['title'], dtf_data['content'])]

lst_corpus_per_ricerca_gruppo = [' '.join([i[0] for i in NER(text) if i[1]=='PERSON' or i[1]=='ORG'])
                              for text in lst_corpus_per_ricerca]

lst_nomi_gruppo_per_ricerca = list(set(lst_nomi_gruppo_puliti))

lst_nomi_gruppo_per_ricerca.remove('')

lst_nomi_gruppo_per_ricerca_sorted = sorted(lst_nomi_gruppo_per_ricerca)

vectorizer = CountVectorizer(vocabulary = lst_nomi_gruppo_per_ricerca_sorted, lowercase=True, binary=True)

dtf_ricerca_gruppo = vectorizer.fit_transform(lst_corpus_per_ricerca_gruppo)

dtf_ricerca_gruppo = pd.DataFrame(dtf_ricerca_gruppo.todense(), columns=lst_nomi_gruppo_per_ricerca_sorted)

dtf_gruppo = pd.concat([dtf_data[['title', 'content']], dtf_ricerca_gruppo], axis=1)

# Export corporate group decode dataframe
pickling_on = open("output/dtf_decodifica_gruppo_corporate","wb")
pickle.dump(dtf_nome_gruppo_pulito[['Gruppo (fonte CIB)','Gruppo (fonte CIB) pulito']], pickling_on)
pickling_on.close()

# Export news profiling dataframe with respect to the corporate group name
pickling_on = open("output/dtf_gruppo","wb")
pickle.dump(dtf_gruppo, pickling_on)
pickling_on.close()