# Import libraries
import pickle
import re
import spacy
import pandas as pd
from spacy import displacy
import en_core_web_sm
nlp = en_core_web_sm.load()
from geotext import GeoText
from sklearn.feature_extraction.text import CountVectorizer

# Define functions
def contentCleaned(text):
    """
    contentCleaned selects the relevant part of the article content

    :param text: String containing article content
    :return: String containing the relevant part of the article content
    """
    try:
        return re.search(r'\(.*Reuters.*\) [\-\–] (.*?) \[\+', text).group(1)
    except AttributeError:
        return ''

def titleCleaned(text):
    """
    titleCleaned selects the relevant part of the article title

    :param text: String containing article tile
    :return: String containing the relevant part of the article title
    """
    text_1 = text
    text_1 = re.sub(r'\bUPDATE\b[\s]?\d+[\s]?\-', '' , text_1)
    text_1 = re.sub(r'\bTABLE\b[\s]?\-', '' , text_1)
    text_1 = re.sub(r'\bCORRECTED\b[\s]?[\-\:][\s]?', '' , text_1)
    text_1 = re.sub(r'\bRPT\b[\s]?[\-][\s]?', '' , text_1)
    text_1 = re.sub(r'\bREFILE\b[\s]?[\-][\s]?', '' , text_1)
    text_1 = re.sub(r'\bCOLUMN\b[\s]?[\-][\s]?', '' , text_1)
    return text_1

def NER(text):
    """
    NER implement named-entity recognition process over a string

    :param text: Input string
    :return: Token classified and classification label tuples list
    """
    doc = nlp(text)
    return [(X.text, X.label_) for X in doc.ents]

# Import the processed news dataframe without duplicates
pickle_off = open("output/dtf_data_newsapi_no_duplicati","rb")
dtf_data = pickle.load(pickle_off)
pickle_off.close()

# Import countries decode dataframe
dtf_decodifica_paese = pd.read_excel(
    'input/dtf_decodifica_paese_20181122.xlsx')

# News profiling with respect to the corporate country
dtf_data.reset_index(drop=True, inplace=True)

lst_corpus_per_ricerca = [titleCleaned(a)+'. '+contentCleaned(b)
                          for a,b in zip(dtf_data['title'], dtf_data['content'])]

lst_corpus_per_ricerca_paese = [' '.join([i[0] for i in NER(text) if i[1]=='GPE' or i[1]=='NORP'])
                                for text in lst_corpus_per_ricerca]

lst_corpus_per_ricerca_paese_2 = [' '.join(list(GeoText(text).country_mentions))
                                  for text in lst_corpus_per_ricerca_paese]

lst_paesi_per_ricerca = list(set(dtf_decodifica_paese['sigla']))

lst_paesi_per_ricerca_sorted = sorted(lst_paesi_per_ricerca)

vectorizer = CountVectorizer(vocabulary = lst_paesi_per_ricerca_sorted, lowercase=False, binary=True)

dtf_ricerca_paese = vectorizer.fit_transform(lst_corpus_per_ricerca_paese_2)

dtf_ricerca_paese = pd.DataFrame(dtf_ricerca_paese.todense(), columns=lst_paesi_per_ricerca_sorted)

dtf_paese = pd.concat([dtf_data[['title', 'content']], dtf_ricerca_paese], axis=1)

# Export corporate country decode dataframe
pickling_on = open("output/dtf_decodifica_paese_corporate","wb")
pickle.dump(dtf_decodifica_paese[['Paese di Residenza / Sede Legale (fonte AdG)', 'sigla']].dropna(),
            pickling_on)
pickling_on.close()

# Export news profiling dataframe with respect to the corporate country
pickling_on = open("output/dtf_paese","wb")
pickle.dump(dtf_paese, pickling_on)
pickling_on.close()