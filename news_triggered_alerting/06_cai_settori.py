# Import libraries
import pandas as pd
import pickle
import re
from nltk import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
wordnet_lemmatizer = WordNetLemmatizer()
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import string

# Define functions
def contentCleaned(text):
    """
    contentCleaned selects the relevant part of the article content

    :param text: String containing article content
    :return: String containing the relevant part of the article content
    """
    try:
        return re.search(r'\(.*Reuters.*\) [\-\–] (.*?) \[\+', text).group(1)
    except AttributeError:
        return ''

def titleCleaned(text):
    """
    titleCleaned selects the relevant part of the article title

    :param text: String containing article tile
    :return: String containing the relevant part of the article title
    """
    text_1 = text
    text_1 = re.sub(r'\bUPDATE\b[\s]?\d+[\s]?\-', '' , text_1)
    text_1 = re.sub(r'\bTABLE\b[\s]?\-', '' , text_1)
    text_1 = re.sub(r'\bCORRECTED\b[\s]?[\-\:][\s]?', '' , text_1)
    text_1 = re.sub(r'\bRPT\b[\s]?[\-][\s]?', '' , text_1)
    text_1 = re.sub(r'\bREFILE\b[\s]?[\-][\s]?', '' , text_1)
    text_1 = re.sub(r'\bCOLUMN\b[\s]?[\-][\s]?', '' , text_1)
    return text_1

def CleanAndLemm(text):
    """
    CleanAndLemm cleans and lemmatizes text

    :param text: String containing text
    :return: String containing the cleaned and lemmatized text
    """
    text = re.sub(r'[\-\–]', ' ' , text)   
    tokens = word_tokenize(text)
    tokens = [w.lower() for w in tokens]
    tokens = [re.sub(r'\.', '' , w) for w in tokens]
    table = str.maketrans('', '', string.punctuation)
    stripped = [w.translate(table) for w in tokens]
    words = [word for word in stripped if word.isalpha()]
    stop_words = set(stopwords.words('english'))
    words = [w for w in words if not w in stop_words]
    return ' '.join(wordnet_lemmatizer.lemmatize(w) for w in words)

# Import the processed news dataframe without duplicates
pickle_off = open("output/dtf_data_newsapi_no_duplicati","rb")
dtf_data = pickle.load(pickle_off)
pickle_off.close()

# Import industry decode dataframe
dtf_decodifica_settore = pd.read_excel(
    'input/DRAFT_News_mappatura Evento_Settore per aggancio news_0.3.xlsx',
    sheetname=1, skiprows=3)

# News profiling with respect to the corporate industry
dtf_data.reset_index(drop=True, inplace=True)

lst_corpus_per_ricerca = [titleCleaned(a)+'. '+contentCleaned(b) for a,b in zip(dtf_data['title'], dtf_data['content'])]

lst_corpus_per_ricerca = [CleanAndLemm(i) for i in lst_corpus_per_ricerca]

dtf_decodifica_settore['puliti'] = [CleanAndLemm(i) for i in list(dtf_decodifica_settore['Decodifica Settore'])]

lst_settori_per_ricerca = list(set(dtf_decodifica_settore['puliti']))

lst_settori_per_ricerca_sorted = sorted(lst_settori_per_ricerca)

vectorizer = CountVectorizer(vocabulary=lst_settori_per_ricerca_sorted, lowercase=True, binary=True)

dtf_ricerca_settori = vectorizer.fit_transform(lst_corpus_per_ricerca)

dtf_ricerca_settori = pd.DataFrame(dtf_ricerca_settori.todense(), columns=lst_settori_per_ricerca_sorted)

dtf_settori = pd.concat([dtf_data[['title', 'content']], dtf_ricerca_settori], axis=1)

# Export news profiling dataframe with respect to the corporate industry
pickling_on = open("output/dtf_settore","wb")
pickle.dump(dtf_settori, pickling_on)
pickling_on.close()

# Export corporate industry decode dataframe
pickling_on = open("output/dtf_decodifica_settore_corporate","wb")
pickle.dump(dtf_decodifica_settore[['Settore (fonte Marketing)', 'puliti']], pickling_on)
pickling_on.close()