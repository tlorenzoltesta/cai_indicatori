# Import libraries
import pickle
import pandas as pd

# Import inputs
pickle_off = open("output/dtf_gruppo","rb")
dtf_gruppo = pickle.load(pickle_off)
pickle_off.close()

pickle_off = open("output/dtf_paese","rb")
dtf_paesi = pickle.load(pickle_off)
pickle_off.close()

pickle_off = open("output/dtf_settore","rb")
dtf_settore = pickle.load(pickle_off)
pickle_off.close()

pickle_off = open("output/dtf_decodifica_gruppo_corporate","rb")
dtf_decodifica_gruppo = pickle.load(pickle_off)
pickle_off.close()

pickle_off = open("output/dtf_decodifica_paese_corporate","rb")
dtf_decodifica_paese = pickle.load(pickle_off)
pickle_off.close()

pickle_off = open("output/dtf_decodifica_settore_corporate","rb")
dtf_decodifica_settore = pickle.load(pickle_off)
pickle_off.close()

pickle_off = open("output/dtf_data_newsapi_no_duplicati","rb")
dtf_data_newsapi_no_duplicati = pickle.load(pickle_off)
pickle_off.close()

dtf_anagrafica = pd.read_excel(
    'input/Anagrafe con dettaglio possesso prodotti GTB 2018.xlsx', dtype = object)

# News profiling
dct_calcolo_settore = (dtf_decodifica_settore.groupby('Settore (fonte Marketing)')
                           .apply(lambda x: list(x['puliti']))
                           .to_dict())

for k,v in dct_calcolo_settore.items():
    dtf_settore[k] = dtf_settore.loc[:,v].max(axis=1)

dtf_settore_1 = dtf_settore[['title', 'content']+list(set(dtf_decodifica_settore['Settore (fonte Marketing)']))]

dtf_profilazione_news = dtf_gruppo.merge(dtf_paesi, how='inner', on=['title', 'content']
                                                     ).merge(dtf_settore_1, how='inner', on=['title', 'content'])

# Corporates profiling
dtf_corporate_gruppo = (
    dtf_anagrafica.merge(dtf_decodifica_gruppo,
                         how='inner',
                         on = 'Gruppo (fonte CIB)'                        
                        )[['Codice SNDG', 'Gruppo (fonte CIB) pulito']]).drop_duplicates()

dtf_corporate_gruppo.columns= ['COD_SNDG', 'DESC_KEY']

dtf_corporate_paese = (
    dtf_anagrafica.merge(dtf_decodifica_paese,
                         how='inner',
                         on = 'Paese di Residenza / Sede Legale (fonte AdG)'                        
                        )[['Codice SNDG', 'sigla']]).drop_duplicates()

dtf_corporate_paese.columns= ['COD_SNDG', 'DESC_KEY']

dtf_corporate_settore = dtf_anagrafica[['Codice SNDG', 'Settore (fonte Marketing)']]

dtf_corporate_settore.columns= ['COD_SNDG', 'DESC_KEY']

dtf_profilazione_corporate = pd.concat([dtf_corporate_gruppo,dtf_corporate_paese,dtf_corporate_settore])

dtf_profilazione_corporate_1 = pd.get_dummies(dtf_profilazione_corporate, columns=['DESC_KEY'],
                                              prefix='', prefix_sep='')

dtf_profilazione_corporate_2 = dtf_profilazione_corporate_1.groupby('COD_SNDG',
                                                                    sort=False, as_index=False).sum()

# Weight vector
dtf_corporate_gruppo['PESO'] = 4

dtf_corporate_paese['PESO'] = 1

dtf_corporate_settore_1 = dtf_corporate_settore.reset_index(drop=True)

dtf_corporate_settore_1['PESO'] = 2

dtf_pesi = pd.concat([dtf_corporate_gruppo[['DESC_KEY','PESO']],
                      dtf_corporate_paese[['DESC_KEY','PESO']],
                      dtf_corporate_settore_1[['DESC_KEY','PESO']]]).drop_duplicates().reset_index(drop=True)

dtf_pesi_t=dtf_pesi.T

dtf_pesi_t.columns = dtf_pesi_t.loc['DESC_KEY']

int_pesi = dtf_pesi_t.drop('DESC_KEY')

# Further column name normalization
str_columns_name = list(set(int_pesi.columns) & set(dtf_profilazione_news.columns) &
                        set(dtf_profilazione_corporate_2.columns))

dtf_profilazione_news_finale = dtf_profilazione_news[['title', 'content']+str_columns_name]

dtf_profilazione_news_finale = pd.merge(dtf_data_newsapi_no_duplicati[['index','title', 'content']],
                                        dtf_profilazione_news_finale,
                                        how='inner', on=['title', 'content'])

dtf_profilazione_news_finale_t = dtf_profilazione_news_finale[['index','title', 'content']+str_columns_name].T

dtf_profilazione_corporate_finale = dtf_profilazione_corporate_2[['COD_SNDG']+str_columns_name]

# Export weight vector
pickling_on = open("output/int_pesi","wb")
pickle.dump(int_pesi[str_columns_name], pickling_on)
pickling_on.close()

# Export news profiling dataframe
pickling_on = open("output/dtf_profilazione_news","wb")
pickle.dump(dtf_profilazione_news_finale_t, pickling_on)
pickling_on.close()

# Export corporate profiling dataframe
pickling_on = open("output/dtf_profilazione_corporate","wb")
pickle.dump(dtf_profilazione_corporate_finale, pickling_on)
pickling_on.close()