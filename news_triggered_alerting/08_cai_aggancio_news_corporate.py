# Import libraries
import pickle
import pandas as pd
import numpy as np
import re

# Import weight vector
pickle_off = open("output/dtf_profilazione_corporate","rb")
dtf_profilazione_corporate_finale = pickle.load(pickle_off)
pickle_off.close()

# Import news profiling dataframe
pickle_off = open("output/dtf_profilazione_news","rb")
dtf_profilazione_news_T_finale = pickle.load(pickle_off)
pickle_off.close()

# Import weight vector
pickle_off = open("output/int_pesi","rb")
int_pesi = pickle.load(pickle_off)
pickle_off.close()

# Match news and corporate
mtr_corporate = dtf_profilazione_corporate_finale.iloc[:,1:].values

mtr_news = dtf_profilazione_news_T_finale.iloc[3:].values

int_pesi = int_pesi.values

int_pesi = int_pesi[0]

mtr_corporate_pesate = np.multiply(mtr_corporate,int_pesi)

mtr_aggancio = np.dot(mtr_corporate_pesate,mtr_news)

dtf_profilazione_news_T_aggancio = dtf_profilazione_news_T_finale.iloc[:3,:]

dtf_news_corporate = pd.DataFrame(mtr_aggancio, index = dtf_profilazione_corporate_finale['COD_SNDG'],
                            columns = pd.MultiIndex.from_arrays(
                                [list(x) for x in dtf_profilazione_news_T_aggancio.values],
                                names=(dtf_profilazione_news_T_aggancio.index)))

dtf_news_corporate_1 = dtf_news_corporate.loc[(dtf_news_corporate>2).any(axis=1)]

dtf_news_corporate_2 = dtf_news_corporate_1.loc[:,(dtf_news_corporate_1>2).any(axis=0)]

dtf_news_corporate_3 = pd.melt(dtf_news_corporate_2.reset_index(), id_vars=['COD_SNDG'])

dtf_news_corporate_4 = dtf_news_corporate_3[dtf_news_corporate_3['value']>2]

dtf_value_binary = dtf_news_corporate_4.value.apply(lambda x: pd.Series(list(bin(x)[2:].zfill(3))))

dtf_value_binary.columns = ['fl_gruppo', 'fl_settore', 'fl_paese']

dtf_news_corporate_5 = dtf_news_corporate_4.merge(dtf_value_binary, how = 'inner', left_index=True,
                                                  right_index=True) 

# Export news corporate matching dataframe
pickling_on = open("output/dtf_news_corporate","wb")
pickle.dump(dtf_news_corporate_5, pickling_on)
pickling_on.close()

dtf_news_corporate_5.to_excel(
    "output/dtf_news_corporate.xlsx",
    sheet_name='Result',
    index=False)