# Stream News Triggered Alerting

## Contents
 * Introduction
 * Graphical Workflow
 * Scripts
    * 01_cai_news_retrival.py
    * 02_cai_news_cleaning.py
    * 03_cai_remove_duplicates.py
    * 04_cai_corporate.py
    * 05_cai_paesi.py
    * 06_cai_settori.py
    * 07_cai_preparazione_aggancio.py
    * 08_cai_aggancio_news_corporate.py
    * 09_cai_prioritizzazione_news.py
 
## Introduction
This document is intended to describe the code of the data processing workflow for the __Stream News Triggered Alerting__.

News Triggered Alerting Model helps to

1. Detect and capture all Relevant Business News to support Sales processes identifying commercial targets

2. Optimize the storytelling during the origination phase reducing the Time To Market of Trade, Cash and Commercial Acquiring business proposals


## Graphical Workflow
![Workflow](/news_triggered_alerting/WorkflowStreamNewsTriggeredAlerting.png)

## 01_cai_news_retrival.py

The first step concerns the news retrieval through a set of keywords, designed along the GdL. This has been implemented by means of the [Python client library](https://github.com/mattlisiv/newsapi-python) of [News API](https://newsapi.org/) Developer account. The result of the retrieval is a list of JSON-like objects that have been then transformed into a dataframe. 

## 02_cai_news_cleaning.py

The dataframe obtained from the previous step has been then processed in order to substitute content with description, where the content was non-informative, and to remove duplicate news and the ones with non-informative content and description. In case of news with the same title and different content or vice-versa, the last news has been kept. The resulting dataframe has been also reshaped.

## 03_cai_remove_duplicates.py

After a title and content cleaning step, the text of title and content has been vectorized. Then a similarity analysis has been carried out to remove almost identical news. Where the cosine similarity value between two news was higher than 0.9, the last news was kept.

## 04_cai_corporate.py

The Corporate group names, retrieved from _Gruppo (fonte CIB)_ column of _Corporate General Data_ table, has been processed in order to extract the relevant part. After a cleaning step of title and content, a named-entity recognition process has been performed on the text to identify ORG (companies, agencies, institutions, etc.) and PERSON (people, including fictional). Finally the processed Corporate group names has been searched in the title and content text resulting from the steps described.

## 05_cai_paesi.py

The Corporate countries, a translation of the registered office country found in _Corporate General Data_ table, has been searched in a similar processed title and content of the news. The main difference is that the named-entity recognition process has been focused on the identification of NORP (nationalities or religious or political groups) and GPE (countries, cities, states). The geographical entities have subsequently been renamed to the country acronyms.

## 06_cai_settori.py

The Corporate industries, a translation and elaboration of the industries names found in _Corporate General Data_ table, has been lemmatized and searched in the cleaned and lemmatized title and content of the news.

## 07_cai_preparazione_aggancio.py

The dataframes, from the previous three steps, have been joined to obtain the _News Profiling_ table. Then it has been constructed the _Corporate Profiling_ table from the _Corporate General Data_ table. A vector of weights has been subsequently built for the matching between news and corporates.

## 08_cai_aggancio_news_corporate.py

Finally, the matching between news and corporates occurs when the name of the corporate group or the industry and country of the corporate is tracked down in the news. This has been implemented through a dot product between the _Corporate Profiling_ table, already multiplied by a vector of weight, and the _News Profiling_ table in order to preserve which of the three components (group name, industry and country) has been found in the news.