## Library import
from tika import parser
import re
from langdetect import detect
import pandas as pd
import collections
import pickle
import glob
import os
import nltk
import sklearn
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
import numpy as np
import treetaggerwrapper
import gensim
from gensim import models
import operator


###################################################### Configuration input ######################################################
path_cartella_pdf = 'input' # financial statements PDF folder path 
lista_codice_fiscale_trade_finance = [] # Corporate Trade Finance VAT number python list
lista_codice_fiscale_non_trade_finance = [] # Corporate Non-Trade Finance VAT python list
#################################################################################################################################


# 1 - Financial Statements Acquisition

## Text parsing through Apache Tika
## [INPUT] = financial statements PDF
## [OUTPUT] = list of financial statements text
rawDocuments_list = []
for input_file in glob.glob(os.path.join(path_cartella_pdf, '*.PDF')):
    doc = {}
    filename = os.path.basename(input_file)
    name = filename.strip('.pdf')
    parsedPDF = parser.from_file(input_file)
    content = parsedPDF["content"]
    doc['id'] = name
    doc['vat_number'] = name[7:18]
    doc['text'] = content
    rawDocuments_list.append(doc)

## Financial statements text export
pickling_on = open("rawDocuments_list","wb")
pickle.dump(rawDocuments_list, pickling_on, protocol=4)
pickling_on.close()

## Financial statements text import
pickle_off = open("rawDocuments_list","rb")
rawDocuments_list = pickle.load(pickle_off)
pickle_off.close()

## Notes to the Financial Statement chapter parsing
## [INPUT] = text, initial chapter string list, final chapter string list
## [OUTPUT] = chapter text
def find_between( s, list_first, list_last ):
    
    first_elem = 0
    first = ''
    for elem in list_first:
        if elem in s and s.index(elem) > first_elem:
            first_elem = s.index(elem)
            first = elem
    if first == '':
        return ""
    last_elem = float('inf')
    last = ''
    for elem in list_last:
        if elem in s and s.index(elem) < last_elem:
            last_elem = s.index(elem)
            last = elem
    if last == '':
        return ""
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""

## Notes to the Financial Statement chapter parsing
## [INPUT] = text, initial chapter string, final chapter string
## [OUTPUT] = chapter text
def find_between_string( s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""

## Notes to the Financial Statement initial chapter string list
section_0 = ['nota integrativa al bilancio di esercizio chiuso', 'nota integrativa/nal bilancio', 'nota integrativa al bilancio al']
section_1 = ['nota integrativa, parte iniziale','nota integrativa parte iniziale']
section_2 = ['nota integrativa attivo','nota integrativa abbreviata, attivo','nota integrativa, attivo','nota integrativa abbreviata attivo']
section_3 = ['nota integrativa passivo e patrimonio netto','nota integrativa abbreviata, passivo e patrimonio netto','nota integrativa, passivo e patrimonio netto','nota integrativa abbreviata passivo e patrimonio netto']
section_4 = ['nota integrativa conto economico','nota integrativa abbreviata, conto economico','nota integrativa, conto economico','nota integrativa abbreviata conto economico']
section_4_end = ['nota integrativa rendiconto finanziario','nota integrativa abbreviata, rendiconto finanziario','nota integrativa, rendiconto finanziario','nota integrativa abbreviata rendiconto finanziario','nota integrativa altre informazioni','nota integrativa abbreviata, altre informazioni','nota integrativa, altre informazioni','nota integrativa abbreviata altre informazioni']
section_5 = ['nota integrativa rendiconto finanziario','nota integrativa abbreviata, rendiconto finanziario','nota integrativa, rendiconto finanziario','nota integrativa abbreviata rendiconto finanziario']
section_6 = ['nota integrativa altre informazioni','nota integrativa abbreviata, altre informazioni','nota integrativa, altre informazioni','nota integrativa abbreviata altre informazioni']
section_7 = ['nota integrativa parte finale','nota integrativa abbreviata, parte finale','nota integrativa, parte finale','nota integrativa abbreviata parte finale', 'dichiarazione di conformità del bilancio']

## Pattern for Annual Report detection
pattern_rsg1 = re.compile(r'\nrelazione.{0,50}gestione.{0,500}(signori|egregi)(.*?)(cda.{0,3}\n|amministratore.{0,2}unico.{0,3}\n|consiglio.{0,2}d.{0,3}amministrazione.{0,3}\n|amministratore.{0,2}delegato.{0,3}\n)',re.DOTALL)
pattern_rsg2 = re.compile(r'\nrelazione.{0,50}gestione.{0,500}(signori|egregi)(.{19360})',re.DOTALL)

## Apply regular expression recursively
## [INPUT] = input text 
## [OUTPUT] = output text
def apply_regexs(regexs, text):
    for k,v in regexs.items():
        text = re.sub(k, v, text)
    return text

regexs = collections.OrderedDict()

regexs [r'\.'] = r''    
regexs [r'[^a-zA-Z0-9]'] = r' '
regexs [r'\d'] = r' '
regexs [r'\b.\b'] = r' '
regexs [r'\s+'] = r' '

## Geographic db import
pickle_off = open("geo_collection","rb")
geo_list_coll = pickle.load(pickle_off)
pickle_off.close()
geo_name_list = [element['name'] for element in geo_list_coll]

## Document information retrieval 
documents_list = []

for elem in rawDocuments_list:
    
    doc = {}
    
    doc['name'] = find_between_string(elem['text'].lower(),'\ndenominazione ','\n')

    doc['vat_number'] = elem['vat_number']
    
    doc['address'] = find_between_string(elem['text'].lower(),'\nindirizzo sede ','\n')
    
    doc['group_name'] = find_between_string(elem['text'].lower(),'\nl´impresa appartiene al gruppo* ','\n')
    
    doc['cciaa'] = find_between_string(elem['text'].lower(),'\nCCIAA/NREA '.lower(),'\n')[:2]
    
    doc['nrea'] = find_between_string(elem['text'].lower(),'\nCCIAA/NREA '.lower(),'\n')[3:]
    
    doc['legal_form'] = find_between_string(elem['text'].lower(),'\nforma giuridica ','\n')
    
    doc['ateco'] = find_between_string(elem['text'].lower(),'\nattività economica ateco 2007 ','\n')
    
    doc['id'] = elem['id']
    
    doc['text'] = elem['text']
    
    doc['iniziale'] = find_between(elem['text'].lower(),section_1,section_2)
    
    doc['attivo'] = find_between(elem['text'].lower(),section_2,section_3)
    
    doc['passivo'] = find_between(elem['text'].lower(),section_3,section_4)
    
    doc['economico'] = find_between(elem['text'].lower(),section_4,section_4_end)
    
    doc['finanziario'] = find_between(elem['text'].lower(),section_5,section_6)
    
    doc['altre_info'] = find_between(elem['text'].lower(),section_6,section_7)
    
    hasParteFinale = False
    for label in section_7:
        if label in elem['text'].lower():
            hasParteFinale = True
    doc['finale'] = hasParteFinale
    
    fatturato = None
    anno = re.findall(r'\d+$', find_between_string(doc['text'].lower(),'\nprincipali voci di bilancio','\n'))
    ammontare = re.findall(r' \S+$', find_between_string(doc['text'].lower(),'\nfatturato','\n'))
    if len(ammontare)+len(anno)>1:
        fatturato = ammontare[0].strip() + ' (' +anno[0].strip() +')'
    doc['fatturato'] = fatturato
    
    doc['establishment_date'] = find_between_string(doc['text'].lower(),'\ndata di costituzione dell´impresa ','\n')
       
    quotata = None
    if find_between_string(doc['text'].lower(),'\nquotazione in borsa ','\n') == 'si':
        quotata = True
    elif find_between_string(doc['text'].lower(),'\nquotazione in borsa ','\n') == 'no':
        quotata = False 
    doc['is_publicity_traded'] = quotata
    
    try:
        doc['relazione_gestione'] = re.search(pattern_rsg1, elem['text'].lower()).group(2)
    except AttributeError:
        try:
            doc['relazione_gestione'] = re.search(pattern_rsg2, elem['text'].lower()).group(2)
        except AttributeError:
            doc['relazione_gestione'] = ''

    isReadable = False
    if ((len(doc['iniziale']) > 1000) + (len(doc['attivo']) > 1000) + (len(doc['passivo']) > 1000) + (len(doc['economico']) > 1000) + (len(doc['finanziario']) > 100) + (len(doc['altre_info']) > 1000) + (hasParteFinale == True)) > 3:
        isReadable = True
    doc['isReadable'] = isReadable   
    
    doc['language'] = None
    text = doc['iniziale'] + doc['attivo'] + doc['passivo']  + doc['economico'] + doc['finanziario'] + doc['altre_info'] + doc['relazione_gestione']
    if len(text) > 100:
        doc['language'] = detect(text)
        
    text_cleaned = apply_regexs(regexs, text)
    countries_names = [] 
    for a in geo_name_list:
        if (a in text_cleaned) and (a not in countries_names):
            countries_names.append(a)
    doc['countries'] = list(set(countries_names))
    
    documents_list.append(doc)

del rawDocuments_list

## Document information export 
pickling_on = open("documents_list","wb")
pickle.dump(documents_list, pickling_on, protocol=4)
pickling_on.close()


# 2 - Financial Statements Text Processing

## Stopword customization
nltk_sw = set(nltk.corpus.stopwords.words('italian'))
nltk_en_sw = set(nltk.corpus.stopwords.words('english'))
nltk_de_sw = set(nltk.corpus.stopwords.words('german'))
other_sw = """pag
essere|stare
essere
avere
pagina
itcc
tassonomia
cc
codice civile
comma
seguito
pari
art
articolo
conforme
altro
questo
generare
data
seguente
tale
secondo
base
primo
senso
anno
bis
così
parte
natura
entro
oltre
stesso
caso
mese
fare
nonché
vario
dato
terzo
ultimo
ogni
dlgs
rif
alcun
alcuno
già
ex
dopo
ter
via
tfr
pario
qualora
certo
mentre
ovvero
conte|conto
alla
""".split()
swc = list(nltk_sw) + list(nltk_en_sw) + list(nltk_de_sw) + other_sw
file = open("stop_words_customized_v2.txt", "w")
for val in swc:
    file.write(str(val) + '\n')
file.close()

## Document information import
pickle_off = open("documents_list","rb")
documents_list = pickle.load(pickle_off)
pickle_off.close()

## Geographic term expansion and bigram unification
df_geo=pd.read_excel('DB_geografico.xlsx', header=0)
df_geo.fillna('', inplace=True)
df_geo[7] = ' ' + df_geo['Termine'] + ' '
df_geo[8] = ' ' + df_geo['Termine_unificato'] + ' '
df_geo[9] = ' ' + df_geo['Termine_unificato'] + ' ' + df_geo['Tipologia'] + ' ' + df_geo['Europa'] + ' '+ df_geo['Continente'] + ' ' + df_geo['Continente_suddivisione'] + ' ' + df_geo['Federazione']
df_geo_dict_1 = df_geo.iloc[:,[7,8]]
df_geo_dict_2 = df_geo.iloc[:,[8,9]]
geo_list_1 = collections.OrderedDict(df_geo_dict_1.to_records(index=False))
geo_list_2 = collections.OrderedDict(df_geo_dict_2.to_records(index=False))
geo_dict_1 = list(geo_list_1.items())
geo_dict_2 = list(geo_list_2.items())

del df_geo
del df_geo_dict_1
del df_geo_dict_2

## Customized stopword import
swc = open("stop_words_customized_v2.txt", "r")
swc = swc.read().split()

## Unified corpus list 
corpus_per_apply_regexs_1 = [x['iniziale'] + x['attivo'] + x['passivo']  + x['economico'] + x['finanziario'] + x['altre_info'] for x in documents_list if x['isReadable'] and x['language'] == 'it']

del documents_list

## Text processing and cleaning
regexs_1 = collections.OrderedDict()

regexs_1 [r'€'] = r'eur' 
regexs_1 [r'\$'] = r'dollaro'  
regexs_1 [r'¥'] = r'yen'      
regexs_1 [r'£'] = r'sterlina'
regexs_1 [r'\.'] = r''    
regexs_1 [r'[^a-zA-Z0-9À-ÖØ-öø-ÿ]'] = r' '
regexs_1 [r'\d'] = r' '
regexs_1 [r'\b.\b'] = r' '
regexs_1 [r'\s+'] = r' '
regexs_1 [r'\b(ue|eu)\b'] = r' unione_europea '
regexs_1 [r'\bus[a]?\b'] = r' stati_uniti '
regexs_1 [r'\bdir\b'] = r' direttiva ' 
regexs_1 [r'\brep\b'] = r' repubblica ' 
regexs_1 [r'\bdollaro( americano| stati uniti| statunitense| stati_uniti| usa)\b'] = r'usd'
regexs_1 [r'\b(stati_uniti |usd )dollaro\b'] = r'usd'
regexs_1 [r'\bsterlina( britannico| britannica| gran bretagna| inglese| uk| gbp)( gbp)?\b'] = r'gbp'
regexs_1 [r'\buk sterlina\b'] = r'gbp'
regexs_1 [r'\b(rmb|renminbi|yuan)( yuan)?( cinese)?\b'] = r'cny'
regexs_1 [r'\b((japanese )?yen( giapponese)?|jpyen)\b'] = r'jpy'

corpus_to_swc = [apply_regexs(regexs_1, doc) for doc in corpus_per_apply_regexs_1]

del corpus_per_apply_regexs_1

## Stopwords remotion
corpus_to_dict_1 = [' '.join([word for word in NIB.split() if word not in swc]) for NIB in corpus_to_swc]

del corpus_to_swc

## Unification of selected terms with more than one word
corpus_to_lemmatize = []
boole=False
for i in corpus_to_dict_1:
        temp=""
        boole=False
        for a, b in geo_dict_1:
            if(a.lower() in i):
                temp=i.replace(a.lower(), b.lower())
                i=temp
                boole=True
        if boole:    
            corpus_to_lemmatize.append(temp)
        else:
            corpus_to_lemmatize.append(i)

del corpus_to_dict_1

## Lemmatization
tagger = treetaggerwrapper.TreeTagger(TAGLANG='it')
tags = [tagger.tag_text(d) for d in corpus_to_lemmatize]
corpus_to_bigram = [[g.split('\t')[2] for g in tags[c]] for c in range(0, len(tags))]

del corpus_to_lemmatize

## Bigram formation 
phrases = models.phrases.Phrases(corpus_to_bigram, threshold=500)
bigram = models.phrases.Phraser(phrases)
corpus_espansione_stati = [' '.join(bigram[corpus_to_bigram[i]]) for i in range(0,len(corpus_to_bigram))]

del corpus_to_bigram

## Geographic terms expansion
corpus_lemmatized = []
boole=False
for i in corpus_espansione_stati:
        temp=""
        boole=False
        for a, b in geo_dict_2:
            if(a.lower() in i):
                temp=i.replace(a.lower(), b.lower())
                i=temp
                boole=True
        if boole:    
            corpus_lemmatized.append(temp)
        else:
            corpus_lemmatized.append(i)

del corpus_espansione_stati

## Further text processing and cleaning
regexs_2 = collections.OrderedDict()

regexs_2 [r'\b.\b'] = r' '
regexs_2 [r'\s+'] = r' '
regexs_2 [r'euro\b'] = r'eur'
regexs_2 [r'\bdollaro( americano| stati uniti| statunitense| stati_uniti| usa)\b'] = r'usd'
regexs_2 [r'\b(stati_uniti |usd )dollaro\b'] = r'usd'
regexs_2 [r'\bsterlina( britannico| britannica| gran bretagna| inglese| uk| gbp)( gbp)?\b'] = r'gbp'
regexs_2 [r'\buk sterlina\b'] = r'gbp'
regexs_2 [r'\b(rmb|renminbi|yuan)( yuan)?( cinese)?\b'] = r'cny'
regexs_2 [r'\b((japanese )?yen( giapponese)?|jpyen)\b'] = r'jpy'

corpora = [apply_regexs(regexs_2, docs) for docs in corpus_lemmatized]

del corpus_lemmatized

## Notes to the Financial Statements corpora export
pickling_on = open("corpora","wb")
pickle.dump(corpora, pickling_on, protocol=4)
pickling_on.close()


# 3 - Feature Extraction

## Notes to the Financial Statements corpora import
pickle_off = open("corpora","rb")
corpora = pickle.load(pickle_off)
pickle_off.close()

## Document information export
pickle_off = open("documents_list","rb")
documents_list = pickle.load(pickle_off)
pickle_off.close()

## Tf-idf matrix computation
vectorizer = TfidfVectorizer(stop_words=swc, ngram_range=(1, 1), max_df=0.99, min_df=0.01, max_features=None)
X = vectorizer.fit_transform(corpora)
feature_names = vectorizer.get_feature_names()
corpus_index = [x['vat_number'] for x in documents_list if x['isReadable'] and x['language'] == 'it']
df_tfidf = pd.DataFrame(X.todense(), index=corpus_index, columns=feature_names)
pickle.dump(vectorizer, open("CAI_TfidfVectorizer.sav", 'wb'))

del X
del corpora
del documents_list

## Tf-idf matrix export
pickling_on = open("df_tfidf","wb")
pickle.dump(df_tfidf, pickling_on, protocol=4)
pickling_on.close()

## Tf-idf matrix import
pickling_off = open("df_tfidf","rb")
df_tfidf = pickle.load(pickling_off)
pickling_off.close()

## Corporate Trade Finance VAT import
documenti_presenti = lista_codice_fiscale_trade_finance

## Tf-idf Trade Finance selection
df_tfidf_tf = df_tfidf[df_tfidf.index.isin(documenti_presenti)]

## Feature selection
vocabolario = {}
for col in df_tfidf:
    vocabolario[col]=df_tfidf[col].mean()
all_docs = sorted(vocabolario.items(), key=operator.itemgetter(1), reverse=True)
all_docs_dict = dict(all_docs)

vocabolario_tf = {}
for col in df_tfidf_tf:
    vocabolario_tf[col]=df_tfidf_tf[col].mean()
tf_docs = sorted(vocabolario_tf.items(), key=operator.itemgetter(1), reverse=True)
tf_docs_dict = dict(tf_docs)

diff = {x: (all_docs_dict[x] - tf_docs_dict[x]) for x in all_docs_dict if x in tf_docs_dict}
features_diff = sorted(diff.items(), key=operator.itemgetter(1), reverse=False)

## List of manually selected features, in case of retraining with new PDFs it must be updated
features = ["spa", "entita_geografica", "extra_ue", "cambio", "europa", "dicembre", "america", "asia",
            "rischio", "usd", "ltd", "prodotto", "nord_america", "controllato", "differenza", "verso",
            "valuta", "fair", "copertura", "mercato", "value", "effetto", "strumento", "gmbh", "paese",
            "materia", "accantonamento", "derivato", "stati_uniti", "svalutazione", "impresa", "vendita",
            "tasso", "group", "tasso_cambio", "acconto", "dollaro", "controllo", "temporaneo", "cambiare",
            "marchio", "imposta", "partecipazione", "leasing", "industriale", "fusione", "de", "inc"]


## Feature selection export
pickling_on = open("features_list","wb")
pickle.dump(features, pickling_on, protocol=4)
pickling_on.close()


# 4 - Target Building

## XGBoost input export
X = df_tfidf[features]
X.reset_index(inplace=True)
X.rename(columns={'index': 'index_tf'}, inplace=True)
documenti_trade_finance = documenti_presenti
documenti_non_trade_finance = lista_codice_fiscale_non_trade_finance
documenti_classificati = documenti_trade_finance + documenti_non_trade_finance
X_classificati = X[X['index_tf'].isin(documenti_classificati)]
X_classificati['label'] = np.where(X_classificati['index_tf'].isin(documenti_trade_finance), 1, 0)
X_non_classificati = X[X['index_tf'].isin(documenti_classificati)==False]
X_classificati.to_csv('training_CAI.csv', sep=';', index=False)
X_non_classificati.to_csv('recall_CAI.csv', sep=';', index=False)