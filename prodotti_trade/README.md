# Stream Prodotti Trade

## Contents
 * Introduction
 * Graphical Workflow
 * Prerequisites and Assumptions
 * Preprocessing_CAI_R2.py
     * 1 - Financial Statements Acquisition
     * 2 - Financial Statements Text Processing
     * 3 - Feature Extraction
     * 4 - Target Building
 * Preprocessing_CAI_new_documents_R2.py
 * XGBoost_CAI_R2.py
     * 5 - Propensity Model Building
 
## Introduction
This document is intended to describe the code of the data processing workflow for the Release 2 of __Stream Prodotti Trade__.

The Stream Prodotti Trade has been designed to analyze the Notes to the Financial Statements in order to point out, through keywords that refer to international trade activities (eg "exchange rate", "foreign currency"), possible areas for development of new business in _Trade Finance_.

The code has been divided into the following two files:

1.  Preprocessing_CAI_R2.py

2.  Preprocessing_CAI_new_documents_R2.py

3.  XGBoost_CAI_R2.py

The first two scripts transform PDF files into dataframes, more specifically _TF-IDF matrices_, that are used as input for the _XGBoost model_, the subject of the last file. Both the preprocessing scripts generate the dataframe used in the recall phase, while just the former produces the input for the training phase.

## Graphical Workflow
![Workflow](/prodotti_trade/WorkflowStreamProdottiTrade_R2.png)

## Prerequisites and Assumptions
To process PDF files are required the following 5 criteria:

1. Financial Statements in Italian language

2. Notes part to the Financial Statement format has to be PDF with selectable text (any other formats or PDF scanning could not be processed within the model for text mining activities by Tika library)

3. PDF Name Structure: Corporate VAT number included between the eighth and eighteenth character of the file name (python code: [7-18], because python starts counting from 0)

4. Matching with at least 4 Financial Statement Notes Sections is required. The seven Financial Statement Notes Sections are defined by the presence of a start_pattern and an end_pattern (with the exception of _Nota Integrativa parte finale_ which has only a start_pattern).

|    Sections PDF                                   |    start_pattern    |    end_pattern    |
|---------------------------------------------------|---------------------|-------------------|
|    Nota Integrativa parte iniziale                |    pattern_1        |    pattern_2      |
|    Nota Integrativa Attivo                        |    pattern_2        |    pattern_3      |
|    Nota Integrativa Passivo e patrimonio netto    |    pattern_3        |    pattern_4      |
|    Nota Integrativa Conto economico               |    pattern_4        |    pattern_5      |
|    Nota Integrativa Rendiconto Finanziario        |    pattern_5        |    pattern_6      |
|    Nota Integrativa Altre Informazioni            |    pattern_6        |    pattern_7      |
|    Nota Integrativa parte finale                  |    pattern_7        |                   |

Reference patterns are defined by the presence of at least one of the corresponding strings, as listed in the following table:

|     Reference pattern     |     Searched strings                                                                                                                                                                                                                                                                                                                                                                              |
|---------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|    pattern_1              |    ['nota integrativa, parte iniziale','nota integrativa   parte iniziale']                                                                                                                                                                                                                                                                                                                       |
|    pattern_2              |    ['nota integrativa attivo','nota integrativa abbreviata,   attivo','nota integrativa, attivo','nota integrativa abbreviata attivo']                                                                                                                                                                                                                                                            |
|    pattern_3              |    ['nota integrativa passivo e patrimonio netto','nota   integrativa abbreviata, passivo e patrimonio netto','nota integrativa,   passivo e patrimonio netto','nota integrativa abbreviata passivo e patrimonio   netto']                                                                                                                                                                        |
|    pattern_4              |    ['nota integrativa conto economico','nota integrativa   abbreviata, conto economico','nota integrativa, conto economico','nota   integrativa abbreviata conto economico']                                                                                                                                                                                                                      |
|    pattern_4_end          |    ['nota integrativa rendiconto finanziario','nota   integrativa abbreviata, rendiconto finanziario','nota integrativa, rendiconto   finanziario','nota integrativa abbreviata rendiconto finanziario','nota integrativa   altre informazioni','nota integrativa abbreviata, altre informazioni','nota   integrativa, altre informazioni','nota integrativa abbreviata altre   informazioni']    |
|    pattern_5              |    ['nota integrativa rendiconto finanziario','nota   integrativa abbreviata, rendiconto finanziario','nota integrativa, rendiconto   finanziario','nota integrativa abbreviata rendiconto finanziario']                                                                                                                                                                                          |
|    pattern_6              |    ['nota integrativa   altre informazioni','nota integrativa abbreviata, altre informazioni','nota   integrativa, altre informazioni','nota integrativa abbreviata altre   informazioni']                                                                                                                                                                                                       |
|    pattern_7              |    ['nota integrativa parte finale','nota integrativa   abbreviata, parte finale','nota integrativa, parte finale','nota integrativa   abbreviata parte finale', 'dichiarazione di conformità del bilancio']                                                                                                                                                                                      |

Each section (with the exception of Nota Integrativa parte finale) should contain a minimum number of characters, depending on the section type, as listed in the following table:

|    Sections PDF                                   |    Characters number                                                                                                                                                                                             |    Reference pattern    |
|---------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------|
|    Nota Integrativa parte iniziale                |    1,000 characters                                                                                                                                                                                              |    pattern 1-2          |
|    Nota Integrativa Attivo                        |    1,000 characters                                                                                                                                                                                              |    pattern 2-3          |
|    Nota Integrativa Passivo e patrimonio netto    |    1,000 characters                                                                                                                                                                                              |    pattern 3-4          |
|    Nota Integrativa Conto economico               |    1,000 characters                                                                                                                                                                                              |    pattern 4-5          |
|    Nota Integrativa Rendiconto Finanziario        |    100 characters                                                                                                                                                                                                |    pattern 5-6          |
|    Nota Integrativa Altre Informazioni            |    1,000 characters                                                                                                                                                                                              |    pattern 6-7          |
|    Nota Integrativa parte finale                  |    the number of   characters has not been checked but only the presence of a string in pattern_7                                                                                                                |    pattern 7            |
|    pattern_7                                      |    ['nota integrativa parte finale','nota integrativa   abbreviata, parte finale','nota integrativa, parte finale','nota integrativa   abbreviata parte finale', 'dichiarazione di conformità del bilancio']     |                         |

## Preprocessing_CAI_R2.py

### 1 - Financial Statements Acquisition
First of all, the text of Corporate Financial Statement has been parsed from the PDF documents. Since most of the documents were in Italian, then only the Financial Statements in this language have been selected. Subsequently, the chapters of the Notes to the Financial Statements and other potentially useful information for Corporate profiling, has been identified in the text parsed. Most of these elements have been discovered through particular patterns in the text analyzed.

The patterns in _Prerequisites and Assumptions_ chapter have been utilized to identify the Notes to the Financial Statements chapters.

In order to select the suitable Financial Statements for the processing, a Note has been considered Readable if it had a significant amount of characters in at least 4 of the compulsory chapters. The number of characters has been identified considering its distribution in the chapters and is shown in _Prerequisites and Assumptions_ chapter.

### 2 - Financial Statements Text Processing
The corpus of the Readable Notes to the Financial Statements, previously extracted, has been elaborated. The customization and removal of the stopwords and the elimination of non-alphanumeric elements have been carried out. To improve model performance, semantic equivalent geographical terms and currencies, crucial for this analysis, has then been unified. For the geographical terms, a semantic expansion has been also performed. Finally, the resulting text has been lemmatized.

### 3 - Feature Extraction
In order to track down the peculiar terms of each document, the corpus of the Notes to the Financial Statements, resulting from the processing step, has been transformed into a term frequency-inverse document frequency (TF-IDF) matrix.  The aim was to find a subset of terms that points out the companies that carry out international trade. Therefore, from the resulting output, 48 terms have been selected among the highest scores evaluated in the following way: the difference between the average value of a column considering only the Corporates involved in Trade Finance (target 1) and the average value of the same column considering all the Corporates (target 1 & 0). This selection has been based on business qualitative criteria.

### 4 - Target Building  
The submatrix of 48 columns previously identified, has been further divided into two other matrices. The first one includes both _Training Set_ and _Test Set_, with an added label column containing the target values, and the second one includes the _Recall Set_.

## Preprocessing_CAI_new_documents_R2.py
This script produces the input to implement Recall in case of new few documents and where it is not required to reconstruct the whole TF-IDF matrix with new statistics.

The output of this script is a table with the Corporate VAT number and the related features columns, selected in the _3 - Feature Extraction_ paragraph of the *Preprocessing_CAI_R2.py* chapter.

### 1 - Financial Statements Acquisition
### 2 - Financial Statements Text Processing
The initial new preprocessing steps are the same as the previous ones described in _1 - Financial Statements Acquisition_  and _2 - Financial Statements Text Processing_ paragraphs of the *Preprocessing_CAI_R2.py* chapter.

### 3 - Recall Input Export
The TF-IDF vectorization has implemented using the statistics of all the Readable documents provided by BE, obtained in the _3 - Feature Extraction_ paragraph of the *Preprocessing_CAI_R2.py* chapter and saved in the stream folder. The features identified in the same paragraph has then be selected and the resulting submatrix has been used for the recall phase.

## XGBoost_CAI_R2.py

### 5 - Propensity Model Building
The XGBoost Model, pre-trained and optimized for the discrimination of companies involved in _Trade Finance_, has been used to predict the target and classify the _Recall Set_, composed of 1,079 Corporates. The model produces a propensity score, normalized between 0 and 1, for each Corporate, in order to detect international trade activities and, consequently, the need for Trade Finance products. Where the score was higher than 0.5, the Corporate has been classified as _Trade Finance_ (target 1).

The *3_11_1 Stimatore prodotti trade* indicator consists of propensity scores described.

The same script can be executed on the preprocessed new documents produced by *Preprocessing_CAI_new_documents_R2.py*.