## Library import
import os
import pandas as pd
import pickle

###################################################### Configuration input ######################################################
path_modello = "" # "CAI_booster_model.sav" model path
csv_input = "recall_CAI.csv" # vectorized csv input
xlsx_output = "Trade_Finance.xlsx" # score output file (BEWARE: it overwrites the existing data)
#################################################################################################################################


# 5 - Propensity Model Building

## Dataset Recall import
recall = pd.read_csv(csv_input, sep=";", dtype={'index_tf':str})

## Pretrained model import
loaded_model = pickle.load(open(path_modello+"CAI_booster_model.sav", 'rb'))
alg = loaded_model

## Classification and score prediction
predictors = ["spa", "entita_geografica", "extra_ue", "cambio", "europa", "dicembre", "america", "asia",
            "rischio", "usd", "ltd", "prodotto", "nord_america", "controllato", "differenza", "verso",
            "valuta", "fair", "copertura", "mercato", "value", "effetto", "strumento", "gmbh", "paese",
            "materia", "accantonamento", "derivato", "stati_uniti", "svalutazione", "impresa", "vendita",
            "tasso", "group", "tasso_cambio", "acconto", "dollaro", "controllo", "temporaneo", "cambiare",
            "marchio", "imposta", "partecipazione", "leasing", "industriale", "fusione", "de", "inc"]
dtest=recall
dtest.is_copy = False
dtest['label'] = alg.predict(dtest[predictors])
dtest['score'] = alg.predict_proba(dtest[predictors])[:, 1]
result_df = dtest[['index_tf', 'label', 'score']]
result_df.columns = ['Codice Fiscale', 'Classificazione Trade Finance', 'Score Trade Finance']

## Result export
writer = pd.ExcelWriter(xlsx_output)
result_df.to_excel(writer,'Score', index = False)
writer.save()